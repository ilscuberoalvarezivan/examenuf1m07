<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [App\Http\Controllers\ProductsController::class, 'all']);
Route::get('/{category}', [App\Http\Controllers\ProductsController::class, 'category']);
Route::get('/score', [App\Http\Controllers\ProductsController::class, 'score']);
//Auth::routes();