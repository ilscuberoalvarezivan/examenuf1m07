<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeQuitScore($query,$score){
        return $this->where('score',"!=",$score);
    }

    public function scopeCategory($query,$category){
        return $this->where('category',$category);
    }
}
