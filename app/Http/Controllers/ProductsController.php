<?php
namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{    
   
    /**
     * Method to list all the products
     */
    public function all(Request $request)
    {   
        $scores=$request->scores;
        return view("examenViews/products",["products" => Product::all(),"categories" => Product::select("unique(category)")->get(),"scores" => $scores]);
    }

    /**
     * Method to list the products filtered by category
     */
    public function category(Request $request, $category){
        $scores=$request->scores;
        return view("examenViews/search",["products" => Product::category($category)->get(),"categories" => Product::select("unique(category)")->get(),"scores" => $scores, "category" => $category]);
    }

    /**
     * Method to list the products filtered by stars
     */
    public function score(Request $request)
    {
        $scores=$request->scores;
        $products=new Product();
        for($i=0;i<5;$i++){
            if($scores[$i]==0){
            $products->quitScore();
            }
        }
        return view("examenViews/score",["products" => $products->get(),"categories" => Product::select("unique(category)")->get(),"scores" => $scores]);
    }
}