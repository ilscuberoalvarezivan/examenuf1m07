<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
    }
}

class ProductSeeder extends Seeder
{
    public function run(){

        DB::table("products")->insert([
            "name"=>"Normal Lego",
            "price"=>80.50,
            "score"=>1,
            "category"=>"lego",
            "img"=>"lego1.jpeg",
            "description"=>"Much lego, such wow",
        ]);
        DB::table("products")->insert([
            "name"=>"Amazing Lego",
            "price"=>8.50,
            "score"=>5,
            "category"=>"lego",
            "img"=>"lego2.jpeg",
            "description"=>"Look at my lego, my lego is amazing",
        ]);
        DB::table("products")->insert([
            "name"=>"Dead lego",
            "price"=>80.50,
            "score"=>4,
            "category"=>"imaginary",
            "img"=>"lego3.jpeg",
            "description"=>"I died in the accident, you have to lego",
        ]);
        DB::table("products")->insert([
            "name"=>"Fake Lego",
            "price"=>80.50,
            "score"=>4,
            "category"=>"fake",
            "img"=>"lego4.jpeg",
            "description"=>"Lego parece, plata no es",
        ]);
    }
}