<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Start Bootstrap</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Register</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

    <div class="col-lg-3">

<h1 class="my-4">Shop Name</h1>
<div class="list-group">
@foreach($categories as $category1)
  <a href="/{{$category}}" class="list-group-item">{{$category}}</a>
  @endforeach
</div>

<form method="GET" action="score">
<div class="form-group">
@for ($i = 0; $i < 5; $i++)
  <div class="form-check">
  <input class="form-check-input" type="checkbox" name="scores[{{$i}}]" id="defaultCheck1" @if(scores[{{$i}}]=="1") checked @endif>
  <label class="form-check-label" for="defaultCheck1">
    {{$i+1}} estrella
  </label>
  </div>
  @endfor
  <!-- Poner 5 checkbox de 1 a 5 estrellas-->
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>

</div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9">
        <h1> Resultados: @for ($i = 0; $i < 5; $i++)
  @if(scores[{{$i}}]=="1") {{$i+1}} estrella @endif
  @endfor
  </h1>

        <div class="row">
        @foreach($products as $product)
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="#"><img class="card-img-top" src="img/{{ $product->img }}" alt=""></a>
              <div class="card-body">
                <h4 class="card-title">
                  <a href="#">{{ $product->name }}</a>
                </h4>
                <h5>{{ $product->price }}</h5>
                <p class="card-text">{{ $product->description }}</p>
              </div>
              <div class="card-footer">
                <small class="text-muted">
                @for ($i = 0; $i < 5; $i++)
                @if($i<$product->score)
                &#9733;
                @else
                &#9734;
                @endif
                @endfor
                </small>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <!-- /.row -->

      </div>
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>


</body>

</html>